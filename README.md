## Description

Query layers/features from a .pmtiles file given a longitude, latitude and zoom

## Run

- `git clone https://gitlab.com/pywkt/pmtiles-feature-lookup.git`
- `cd pmtiles-feature-lookup && npm install`
- `echo "URL=<url-to-pmtiles-file>" >> .env`
- `npm start`

Edit the longitude/latitude coords in `index.js` to change tile lookup
