const pmtiles = require('pmtiles');
const VectorTile = require('@mapbox/vector-tile').VectorTile;
const Pbf = require('pbf');
const latLonToTile = require('./util/latLonToTile');
require('dotenv').config();

async function getFeaturesForLocation(tileX, tileY, zoom) {
  const source = new pmtiles.FetchSource(process.env.URL);
  const mapData = new pmtiles.PMTiles(source);
  const tile = await mapData.getZxy(zoom, tileX, tileY);
  let tileData = new VectorTile(new Pbf(tile.data));

  const waterLayer = tileData.layers['water'];

  for (let i = 0; i < waterLayer.length; i++) {
    let featureName = waterLayer.feature(i).properties.name;
    if (featureName) {
      return featureName;
    }
  }
}

// Helper data to avoid having to copy/paste coords
const locData = {
  leeds01: [53.784244, -1.522884, 15], // Leeds - River Aire
  leeds02: [53.769123, -1.471161, 15], // Leeds - Skeleton Lake
  leeds03: [53.784244, -1.522884, 15], // Leeds - Not water
  leeds04: [53.763293, -1.469448, 15], // Leeds - Not water
};

const locName = 'leeds02';

const getData = async () => {
  const tileToFind = await latLonToTile(
    locData[locName][0],
    locData[locName][1],
    locData[locName][2],
  );

  const res = await getFeaturesForLocation(
    tileToFind[0],
    tileToFind[1],
    tileToFind[2],
  );

  console.log('tile:', tileToFind);
  console.log('res:', res);
};


getData();
