async function latLonToTile(lat, lon, zoom) {
  var latRad = (lat * Math.PI) / 180;
  var n = Math.pow(2, zoom);
  var x = Math.floor(((lon + 180) / 360) * n);
  var y = Math.floor(
    ((1 - Math.log(Math.tan(latRad) + 1 / Math.cos(latRad)) / Math.PI) / 2) * n,
  );

  if (x < 0) x = 0;
  if (x >= n) x = n - 1;
  if (y < 0) y = 0;
  if (y >= n) y = n - 1;

  return [x, y, zoom];
}

module.exports = latLonToTile;
