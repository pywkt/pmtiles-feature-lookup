/**
 * This file is just for testing/exploring
 */

// const pmtiles = require('pmtiles');
// const VectorTile = require('@mapbox/vector-tile').VectorTile;
// const Pbf = require('pbf');
// const latLonToTile = require('latLonToTile');
// require('dotenv').config()

// async function getFeaturesForLocation(tileX, tileY, zoom) {
// console.log('tileX, tileY:', tileX, tileY);
// const source = new pmtiles.FetchSource(process.env.URL);

// const data = new pmtiles.PMTiles(source);
// console.log('data:', data);

// const tile = await data.getZxy(zoom, tileX, tileY);
// console.log('tile:', tile);

// const tileId = pmtiles.zxyToTileId(zoom, tileX, tileY);
// console.log('tileId:', tileId);
//
// const zxy = pmtiles.tileIdToZxy(tileId);
// console.log('zxy:', zxy);

// const header = await data.getHeader();
// console.log('header:', header);

// const metadata = await data.getMetadata();
// console.log('metadata:', metadata);

// const decompressed = await data.decompress(tile.data, 0);
// console.log('decompressed:', decompressed);

// const metadataAttempt = await data.getMetadataAttempt();
// console.log('metadataAttempt:', metadataAttempt);

// const resolvedValueCache = new pmtiles.ResolvedValueCache(1);
// console.log('resolvedValueCache:', resolvedValueCache);

// let temp = new VectorTile(new Pbf(tile.data));
// console.log('temp:', temp);

// const waterLayer = temp.layers['water'];
// console.log('waterLayer:', waterLayer.feature(0));

// const waterTest = temp.layers.water
// console.log('waterTest:', waterTest)

// console.log('waterLayer:', waterLayer)
// for (let i = 0; i < waterLayer.length; i++) {
//   let featureName = waterLayer.feature(i).properties.name;
//   if (featureName) {
//     console.log(featureName); // will print the name of the water body
//     return featureName
//   }
// }

// const buf = Buffer.from(tile.data);
// console.log('buf:', buf.toString())

// const fff = pmtiles.bytesToHeader(decompressed, header.etag)
// console.log('fff:', fff)

// const tiles = await pmtiles.getTilesForLocation(longitude, latitude);
// const tiles = await data.getZxy();
// console.log('tiles:', tiles);

// const tile = tiles.data;
// console.log('tile:', tile);
// const buf = Buffer.from(tile);
// console.log('buf:', buf.toString())
// const features = pmtiles.bytesToHeader(tile)
// return features;
// }

// const runStuff = async () => {
//   const tileToFind = await latLonToTile(53.784244,-1.522884, 15)
//   const mapData = await getFeaturesForLocation(
//     tileToFind[1],
//     tileToFind[2],
//     tileToFind[0],
//   );
//
//   console.log('tileToFind:', tileToFind);
//   console.log('mapData:', mapData)
// };
//
// runStuff();
